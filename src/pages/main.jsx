import React from "react";
import "../styles/main.scss";
import { NavigationBar } from '../components'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

export const Main = () => {
  return (
    <div className="background"> 
      <nav className="navigation-bar">
        <NavigationBar />
      </nav>
      <main>
          
      </main>
    </div>
  );
};
