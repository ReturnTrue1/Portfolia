import React from "react";

export const NavigationBar = () => {
  return (
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">
        Logo
      </a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li>
          <a href="sass.html">About</a>
        </li>
        <li>
          <a href="badges.html">Skills</a>
        </li>
        <li>
          <a href="collapsible.html">Contact</a>
        </li>
      </ul>
    </div>
  );
};
