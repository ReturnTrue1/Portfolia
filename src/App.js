import React from "react";
import "./styles/main.scss";
import { NavigationBar } from './components'
import { Main } from './pages'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route link="/">
          <Main/>
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
